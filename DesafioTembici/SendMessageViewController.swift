import UIKit

class SendMessageViewController: UIViewController {

    @IBOutlet weak var btSend: UIButton!
    @IBOutlet weak var tfMessage: UITextField!
    
    var message: String = ""
    var name: String = "TerraLab"
    var userName: String = "TerraBici"
        
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func sendMessage(_ sender: UIButton) {
        if let messageReceived = tfMessage.text {
            message = messageReceived
        }
        
        request()
    }
    
    func request() {
        let route = URL(string: "")!
        var request = URLRequest(url: route)
        request.httpMethod = "POST"
        
        let body: [String: String] = ["message": message, "name": name, "userName": userName]
        
        do {
            let jsonEncoder = JSONEncoder()
            let body = try jsonEncoder.encode(body)
            request.httpBody = body
        } catch let error {
            print(error.localizedDescription)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept-Type")
        
        
        let task = URLSession.shared.dataTask(with: request) { [self] data, response, error in
            guard let httpResponse = response as? HTTPURLResponse else { return }
            
            if httpResponse.statusCode == 200 {
                DispatchQueue.main.async {
                    navigationController?.popViewController(animated: true)
                }
            }
        }
        task.resume()
    }
}
