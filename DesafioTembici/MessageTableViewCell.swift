import UIKit

class MessageTableViewCell: UITableViewCell {

    @IBOutlet weak var tfTitle: UILabel!
    @IBOutlet weak var tfSubTitle: UILabel!
    
    var message: String = ""
    var userName: String = ""
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
     super.init(style: .subtitle, reuseIdentifier: "cell")

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configuration(message: String, userName: String){
        textLabel?.text = message
        detailTextLabel?.text = userName
    }
}
